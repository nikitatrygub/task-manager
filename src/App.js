import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import TaskBoard from "./containers/TaskBoard";
class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route key="0" exact path="/" render={props => <TaskBoard title="My day" filter="myday" {...props} />} />
          <Route key="1" exact path="/important" render={props => <TaskBoard title="Important" filter="important" {...props} />} />
          <Route key="2" exact path="/planned" render={props => <TaskBoard title="Planned" filter="planned" {...props} />} />
          <Route key="3" exact path="/tasks" render={props => <TaskBoard title="Tasks" filter="" {...props} />} />
        </Switch>
      </Router>
    );
  }
}

export default App;
