import React, { Component } from "react";
import { Layout } from "../components/layout/Layout";
import { bindActionCreators } from "redux";
import { createTask, setTasks, getTask } from "../store/actions";
import { connect } from "react-redux";
import { db } from "../database";
import cuid from "cuid";

const TaskBoardHeader = ({ title }) => (
  <div className="taskboard-header">
    <span className="taskboard-header__title">{title}</span>
  </div>
);

class Task extends Component {
  state = {
    important: false
  };

  componentDidMount() {
    this.setState({ important: this.props.task.important });
  }

  makeImportant = (id, event) => {
    this.setState({ important: !this.state.important });
    let important = !this.state.important;
    let tasks = this.props.tasks;
    let task = tasks.find(t => {
      return t.id === id;
    });
    task.important = important;
    let serializeTasks = JSON.stringify({ tasks: tasks });
    db.setItem("tasks", serializeTasks);
    event.stopPropagation();
  };

  render() {
    const { name, id } = this.props.task;
    const { important } = this.state;
    return (
      <div onClick={() => this.props.onClick(id)} key={id} className="task">
        <span className="task__circle" />
        <span className="task__name">{name}</span>
        <span
          onClick={event => this.makeImportant(id, event)}
          className="task__star"
        >
          {important ? (
            <img src="assets/img/star-full.svg" alt="star" />
          ) : (
            <img src="assets/img/star.svg" alt="star" />
          )}
        </span>
      </div>
    );
  }
}

class TaskBoard extends Component {
  state = {
    name: "",
    openTask: false
  };
  componentDidMount() {
    let tasks = db.getItem("tasks");
    if (tasks) {
      let serializeTasks = JSON.parse(tasks);
      this.props.setTasks(serializeTasks.tasks);
    }
  }

  toggleTask = id => {
    this.setState({ openTask: !this.state.openTask });
    this.props.getTask(id);
  };

  createNewTask = () => {
    let task = {
      id: cuid(),
      name: this.state.name,
      notes: "",
      completed: false,
      filter: this.props.filter,
      important: false
    };
    let oldTasks = this.props.tasks;
    let newTasks = {
      tasks: [...oldTasks, task]
    };
    let serializeNewTasks = JSON.stringify(newTasks);
    db.setItem("tasks", serializeNewTasks);
    this.props.createTask(task);
    this.setState({ name: "" });
  };
  render() {
    const { title, tasks } = this.props;
    const { name, openTask } = this.state;
    const showCreateButton = name.length > 0;
    let filterTasksByFilter = arr =>
      arr.filter(task => {
        if (this.props.filter.length === 0) {
          return task;
        }
        return task.filter === this.props.filter;
      });
    let filteredTasks = filterTasksByFilter(tasks);
    return (
      <Layout openTask={openTask} onClick={this.toggleTask}>
        <div className={`taskboard ${openTask ? "collapsed" : ""}`}>
          <TaskBoardHeader title={title} />
          <div className="taskboard-tasks">
            {filteredTasks.map(task => (
              <Task
                task={task}
                key={task.id}
                tasks={tasks}
                onClick={this.toggleTask}
              />
            ))}

            <div className="taskboard-tasks__input-wrapper">
              <input
                className="taskboard-tasks__input"
                type="text"
                placeholder="Create task"
                value={name}
                onChange={({ target: { value } }) =>
                  this.setState({ name: value })
                }
                onKeyPress={e => e.key === "Enter" && this.createNewTask()}
              />
              {showCreateButton && (
                <button
                  onClick={this.createNewTask}
                  className="taskboard-tasks__create-button"
                >
                  Enter
                </button>
              )}
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks
});

const mapDispatchToState = dispatch => ({
  createTask: bindActionCreators(createTask, dispatch),
  setTasks: bindActionCreators(setTasks, dispatch),
  getTask: bindActionCreators(getTask, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToState
)(TaskBoard);
