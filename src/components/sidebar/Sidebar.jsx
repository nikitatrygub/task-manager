import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

class Sidebar extends Component {
  render() {
    const path = this.props.location.pathname;
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <Link
            className={`sidebar-nav__link ${path === "/" ? "active" : ""}`}
            to="/"
          >
            My day
          </Link>
          <Link
            className={`sidebar-nav__link ${
              path === "/important" ? "active" : ""
            }`}
            to="/important"
          >
            Important
          </Link>
          <Link
            className={`sidebar-nav__link ${
              path === "/planned" ? "active" : ""
            }`}
            to="/planned"
          >
            Planned
          </Link>
          <Link
            className={`sidebar-nav__link ${path === "/tasks" ? "active" : ""}`}
            to="/tasks"
          >
            Tasks
          </Link>
        </nav>
      </div>
    );
  }
}

export default withRouter(Sidebar);
