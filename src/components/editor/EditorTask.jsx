import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateTask } from "../../store/actions";
import { db } from "../../database";

class EditorTask extends Component {
  state = {
    id: "",
    name: ""
  };

  componentDidMount() {
    this.setState(this.props.task);
  }

  updateTaskInDbAndStore = task => {
    this.props.updateTask(task);
    let getTasks = db.getItem("tasks");
    let serializeTasks = JSON.parse(getTasks);
    let tasks = serializeTasks.tasks;
    let taskIndex = tasks.findIndex(t => t.id === task.id);
    let findedTask = tasks[taskIndex];

    let newTask = {
      ...findedTask,
      ...this.state
    };
    let filter = arr => arr.filter(t => t.id !== task.id);
    let filteredTasks = filter(tasks);
    filteredTasks.splice(taskIndex, 0, newTask);

    let newTasks = { tasks: [...filteredTasks] };
    let serializeNewTasks = JSON.stringify(newTasks);
    db.setItem("tasks", serializeNewTasks);
  };

  render() {
    const { name, notes } = this.state;
    let task = this.state;
    return (
      <div className="task-edit">
        <input
          className="task-edit__name"
          type="text"
          value={name}
          onChange={e => this.setState({ name: e.target.value })}
          onKeyPress={e =>
            e.key === "Enter" && this.updateTaskInDbAndStore(task)
          }
        />
        <textarea
          className="task-edit__notes"
          type="text"
          value={notes}
          placeholder="Notes"
          onChange={e => this.setState({ notes: e.target.value })}
          onKeyPress={e =>
            e.key === "Enter" && this.updateTaskInDbAndStore(task)
          }
        />
        <button
          className="task-edit__save"
          onClick={() => this.updateTaskInDbAndStore(task)}
        >
          Save
        </button>
        <div className="task-edit__bottom-panel">
          <button onClick={this.props.onClick} className="task-edit__hide">
            <img src="assets/img/right-arrow.svg" alt="" />
          </button>
          <button className="task-edit__delete">
            <img src="assets/img/delete-button.svg" alt="" />
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  task: state.task
});

const mapDispatchToState = dispatch => ({
  updateTask: bindActionCreators(updateTask, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToState
)(EditorTask);
