import React, { Fragment } from "react";
import Sidebar from "../sidebar/Sidebar";
import EditorTask from "../editor/EditorTask";

export const Layout = ({ children, openTask, onClick }) => (
  <Fragment>
    <Sidebar />
    {children}
    {openTask && <EditorTask onClick={onClick} />}
  </Fragment>
);
