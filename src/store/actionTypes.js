export const CREATE_TASK = "CREATE_TASK";
export const SET_TASKS = "SET_TASKS";
export const GET_TASK = "GET_TASK";
export const UPDATE_TASK = "UPDATE_TASK";