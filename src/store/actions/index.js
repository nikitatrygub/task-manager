import { CREATE_TASK, SET_TASKS, GET_TASK, UPDATE_TASK } from "../actionTypes";

const createTask = todo => ({
  type: CREATE_TASK,
  payload: todo
});

const setTasks = tasks => ({
  type: SET_TASKS,
  payload: tasks
});

const getTask = id => ({
  type: GET_TASK,
  payload: id
});

const updateTask = task => ({
  type: UPDATE_TASK,
  payload: task
});

export { createTask, setTasks, getTask , updateTask };
