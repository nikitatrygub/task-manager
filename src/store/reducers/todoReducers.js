import { CREATE_TASK, SET_TASKS, GET_TASK, UPDATE_TASK } from "../actionTypes";

const initialState = {
  tasks: []
};

export const taskReducers = tasks;

function tasks(state = initialState, action) {
  switch (action.type) {
    case CREATE_TASK: {
      let oldTasks = state.tasks;
      let newState = { ...state, tasks: [...oldTasks, action.payload] };
      return newState;
    }
    case SET_TASKS: {
      return { ...state, tasks: action.payload };
    }
    case GET_TASK: {
      let taskId = action.payload;
      let tasks = state.tasks;
      let task = tasks.find(t => {
        return t.id === taskId;
      });
      return { ...state, task: task };
    }
    case UPDATE_TASK: {
      let taskId = action.payload.id;
      let tasks = state.tasks;
      let taskIndex = tasks.findIndex(t=>t.id === taskId);
      let task = tasks[taskIndex]
      let newTask = {
        ...task,
        ...action.payload
      };
      let filter = arr => arr.filter(task => task.id !== taskId);
      let filteredTasks = filter(tasks);
      filteredTasks.splice(taskIndex,0,newTask)
      return { ...state, tasks: [...filteredTasks] };
    }
    default:
      return state;
  }
}
